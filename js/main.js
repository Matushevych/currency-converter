const fromCurrency = document.getElementById('from-currency');
const toCurrency = document.getElementById('to-currency');
const submitBtn = document.querySelector('.convert-btn');
const textResult = document.querySelector('.result-text');
const conversionRates = document.getElementsByClassName('conversion-rates');
let amount = document.getElementById('amount');
let result = 0;

function getCurrencyData(url) {
    return new Promise((resolve, reject) => {
        const req = new XMLHttpRequest();
        req.open('GET', url);
        req.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (this.status === 200) {
                    resolve(JSON.parse(this.response));
                } else {
                    reject("Can not get currency data");
                }
            }
        };
        req.send();
    })
}

getCurrencyData('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json')
    .then(data => {
        let currencyNames = [];
        let currencyRates = [];
        data.map(item =>{
            currencyNames.push(item.cc);
            currencyRates.push(item.rate)
        });
        createOptions(currencyNames, currencyRates);
    })
    .catch(error => {
    alert(error);
    });

function createOptions(currencyNames, currencyRates){
    for (let i = 0; i < currencyNames.length; i++) {
        let newOption = document.createElement('option');
        newOption.value = currencyRates[i];
        newOption.setAttribute("title", `${currencyNames[i]}`);
        newOption.innerHTML = `${currencyNames[i]}`;
        fromCurrency.appendChild(newOption);
        toCurrency.appendChild(newOption.cloneNode(true));
    }
}

submitBtn.addEventListener('click', conversion);

function conversion() {
    amount = amount.value;
    if (amount > 0 && !isNaN(amount)) {
        convertResult(amount);
        showResult(convertResult(amount), amount);
    }
    else {
        alert("Write only positive numbers in the 'amount' input!")
    }
    clearAmountValue()
}

function convertResult(amount) {
    let fromVal = fromCurrency.options[fromCurrency.selectedIndex].value;
    let toVal = toCurrency.options[toCurrency.selectedIndex].value;
    return result = (fromVal / toVal * amount).toFixed(2);
}

function showResult(convertResult, amount) {
    let fromValAttr = fromCurrency.options[fromCurrency.selectedIndex].getAttribute('title');
    let toValAttr = toCurrency.options[toCurrency.selectedIndex].getAttribute('title');
    let fromVal = fromCurrency.options[fromCurrency.selectedIndex].value;
    let toVal = toCurrency.options[toCurrency.selectedIndex].value;
    textResult.innerHTML = `${amount} ${fromValAttr} = <span class="big">${convertResult} ${toValAttr}</span>`;
    conversionRates[0].innerHTML = `1 ${toValAttr} = ${(toVal / fromVal).toFixed(6)} ${fromValAttr}`;
    conversionRates[1].innerHTML = `1 ${fromValAttr} = ${(fromVal / toVal).toFixed(6)} ${toValAttr}`;
}

function clearAmountValue() {
    amount = document.getElementById('amount');
}